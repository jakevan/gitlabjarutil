# GitLabJarUtil

Very simple singular class that can get the latest job of a branch and download a jar (or any other file), from inside the uploaded artifact zip.

Requires:
 - Apache Commons IO
 - Google GSON
 
 For now, will probably make it plain java if need be.